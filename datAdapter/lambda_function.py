import json
import DataAdapter
import boto3


# Crea un cliente SNS
sns_client= boto3.client('sns', region_name='us-east-1') #TODO.adjust
response = sns_client.create_topic(Name='dataAdapter')#TODO.adjust
topic_arn = 'arn:aws:sns:us-east-1:187031430837:dataAdapter'


def lambda_handler(event, context):

    print('event: ',event)
    
    #event=str(event)
    #event_message = event
    event_message = json.dumps(event)
    print('event: ',event_message)
    #print('event_message:',event_message)
    outMessage=DataAdapter.adapt(event_message)
    print('out_message=',outMessage)
    lambda_client=boto3.client('lambda')
    lambda_payload=json.dumps(outMessage[0])
    lambda_client.invoke(FunctionName='persistencia_Maestra',InvocationType='RequestResponse',Payload=lambda_payload)
    #response=sns_client.publish(TopicArn=topic_arn,Message=outMessage)
    #print("Out:", response['MessageId'],' Message:',outMessage)
    
    return {
        'statusCode': 200,
        'body': '{"key1": "value1"}'
     }