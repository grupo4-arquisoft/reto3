import csv
import json

def adapt(db_data):
    field_order = ["id", "tipoDoc", "numDoc","primerNombre","segundoNombre","apellido1","apellido2","genero","correo","numeroTel","numeroCel"]
    concatenate_fields_name = ["primerNombre", "segundoNombre","apellido1","apellido2"]
    
    db_reader = csv.reader(db_data.splitlines(), delimiter='|')
    json_data = []
        
    for row in db_reader:
        json_item = {}
        for i, field_name in enumerate(field_order):
            if field_name == "id":
                json_item["idCliente"] = row[i]
                
            elif field_name == "numDoc":
                json_item["identificacion"] = row[i]
                
            elif field_name in concatenate_fields_name:
                concatenated_value = " ".join([row[field_order.index(field)] for field in concatenate_fields_name])
                json_item["nombre"] = concatenated_value
                    
            elif field_name == "correo":
                json_item["email"] = row[i]
                    
            elif field_name == "numeroTel":
                json_item["telefono"] = row[i]
                    
        json_item["direccion"] = " "
        json_item["tipoCliente"] = " "
            
        json_data.append(json_item)
            
        return json_data
    